###############################################################################
# This file is part of the cfg-maxiv-checkup project.
#
# Copyright Lund University
#
# Distributed under the GNU GPLv3 license. See LICENSE file for more info.
###############################################################################


def test_top_level_import_has_version():
    import tango_checkup as package

    assert hasattr(package, "__version__")
